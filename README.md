# MdfViewer

## Getting started
The aim of the GUI "MdfViewer" is to enable the contents of database tables to be viewed as easily and quickly as possible. 
- For this purpose, a button is generated for each table, which displays its content when clicked.
- The database diagram can be displayed in a different window.
- In addition, you can enter SQL queries in a "query" text box, which is supported by autocomplete.
- In the basic version, SQLServer and SQLite databases are available. For other database types (see below), additional DLLs must be copied.

### Database types
The following databases are available:
- SqlServer MDF files
- Sqlite files
- Postgres
- MySql

## Main Window
![Main Window](images/main_window.png)

In the DataGrid you can 
- edit
- add
- delete

No explicit saving is necessary, i.e. the changes are automatically transferred to the database.
### Features of Main Window
|Feature   |Description |
|:---------|:-----------|
|Query| Here you can enter SQL selects. This is supported by Intellisense (selection with cursor keys or ```<tab>```): ![Queries](images/queries_screenshot.png)
|Tables| A button is created for each table. If you click on the button, the data is displayed. In case of larger tables, only 50 data records are displayed at first and the others are loaded when scrolling down.
|Status bar| Displays the following: <br>- Number of records <br>- Current ConnectionString <br>- Checkbox to hide the table info
|Table-Info| Shows the short info for all columns of the currently opened table. <br>- The primary key is shown in *italics*.<br>- Identity columns are shown in <span style="color:red">red</span>
|![Open Database](images/open.png){width=40px height=40px}|	Opens a database. The path of the last database loaded is saved and proposed the next time. The full file name of the MDF file is displayed next to it.
|Drag&Drop| Instead of "Open", you can also drag a database file onto the window so that its data is displayed.
|Version| Displays the database version of the currently opened database.
|Version Check| If you drag a database file (other than the one currently open) onto the "Version" label, the database version of this file is displayed next to it. The background turns blue. <br> ![Version Check](images/version_check.png)
|![Show Diagram](images/show_diagram.png){width=40px height=40px}| Show diagram (see below)
|![Execute SQL](images/sql_execute.png){width=40px height=40px}| Execute SQL commands (see below)
|![SQL History](images/sql_history.png){width=40px height=40px}| History of SQL commands
|![Excel export](images/excel_download.png){width=40px height=40px} | Export all tables to an Excel workbook

### Diagram
The database model can be displayed graphically in a separate window. 
![Diagram Window](images/diagram_window.png)
The tables are arranged in such a way that as few/short connecting lines as possible are created. 
- Depending on the number of tables, this is done by iterating all permutations or using a genetic algorithm.
- The diagram can be adapted (delete and move tables, as well as zoom with ```<Ctrl>```+mouse wheel)
- The diagram can be printed as a png (the file is saved in the same directory as the database).
- At the beginning, only those tables are displayed that also have relationships/foreign keys to other tables. If you click on "Show all", all tables are taken into account.
- Primary keys are displayed with a blue font at the top of a table 
- Identity columns are displayed in red font
- Foreign keys are displayed with a skyblue background at the bottom.

### Excel
Click on the “Excel” button to export the database to Excel, whereby a separate worksheet is created for each table. References are resolved in columns marked in blue.


## SqlExecutor 
Here you can enter several general SQL statements. Click on **Execute** to execute them. In the event of an error, a tooltip is displayed above the error.
**Clear** deletes all entries.

![SQLExecutor Window](images/sqlexecutor_window.png)

With "Open SQL-file" you can load a text file with SQL statements.

## SqlHistory 
The last 20 select statements in the query text field are saved (each per open database) and can be executed again:

![SQLHistory Window](images/sqlhistory_window.png)

- **Double-click**: Executes statement directly
- **Click**: copies the statement into query text field


## Version-History
|version   |changes |
|:---------|:-----------|
|8.1.1| Adjust layout (Grid on top)
|8.1.0| Migrate: Migrate to .Net 8 and fix all warnings and messages
|7.7.3| Bugfix: Handle reserved column names - currently from and to are handled
|     | Bugfix: Not required? Update database row with columns like Book_Id (instead of BookId)
|7.7.2| Excel export: format timestamps as date and open Excel when export is finished
|7.7.1| Added foreign key lookups to Excel export
|7.7.0| Added button to export database to Excel (one sheet per table)
|7.6.1| Bugfix: Identity column detected also, if IsAutoIncrement set to true
|7.6.0| Bugfix: Problem fixed if updating row with DateTime column null
|7.5.0|	Logging introduced
|7.4.0|	Bugfix: detecting nullable foreign keys
|7.3.1|	Bugfix: detecting foreign keys in SqlServer (TargetColumn)
|7.3.0|	Bugfix: use schema name for queries in SqlServer
|7.2.3|	Bugfix: detecting foreign keys SQLServer
|7.2.2|	Bugfix: detecting foreign keys
|7.2.1|	ChartOptimizerFactory introduced: Factory method that chooses between permutations and genetic algorithm according to number of tables
|7.2.0|	genetic algorithm introduced for arranging database diagram
|7.1.0| switched to .net6 <br>support for .sdf files cancelled <br>bugfix: insert for non-identity columns now possible <br>bugfix: columns with types like Byte[] now are ignored when generating insert statement
|6.4.1| Detect and display Identity columns in Chart and table description (in red colour, respectively)
|6.3.5| Bugix: ForeignKeys in SQLite: e.g. Kino_KinoId --> KinoKinoId
|6.3.4| Bugfix when recognising existing DLLs
|6.3.3| Icons for buttons
|6.3.2| Performance improvement Auto-Arrange
|6.3.1| Automatically arrange tables so that there are as few/short connecting lines as possible.
|6.2.1| Check if DLLs for database providers are available
|6.2.0| Display Foreign Keys at the end of each table
|6.1.0| Bugfix display database version. <br>Database diagram for Mdf and Sqlite<br/>
|5.2.0| Added access to MySql databases (untested)
|5.1.1| Bugfix for Postgres
|5.1.0| Added access for Postgres databases
|4.2.3| Output lines in SqlExecutor can be scrolled 
|4.2.2| Bugfix: Double click on .sqlite, .db or .sdf now also opens these files
|4.2.1| Bugfix Drag&Drop for Sdf/Sqlite
|4.2.0| Access class for Sqlite database files
|4.1.0| Access class for SqlServer Compact (*.sdf)
|4.0.0| Redesign of DB access classes (preparation for Factory)
|3.6.1| Outputs are now visible when starting from the console. <br>In case of exceptions, all exceptions are displayed.<br/>
